# Django Challenge

**Desafio para candidatos a estagiário no Aplicativo Farma Já como Fullstack Developer**

**Desafio:** Buscador de Notas de Filmes no OMDB

Para este desafio você terá que consumir a API da OMDB (http://www.omdbapi.com/) e apresentar o resultado em uma lista atendendo os seguintes critérios:

    Deve ser implementado com Django;
    A landing page deve conter um campo de busca;
    O resultado da busca deve ser exibido em cards, na mesma página ou numa página de resultado;
    Os cards devem conter apenas o poster e o nome de cada filme;
    Ao selecionar um card, um modal deve ser exibido;
    O modal deve conter o poster, o título, a nota do filme, o ano, a sinopse e os prêmios caso possua;
    A API pode ser chamada com uma chave gratuita criada no próprio site;

Sinta-se livre na escolha de arquitetura, libs, layout e testes. Para entregar o desafio, faça um Fork deste repositório, adicione seus arquivos e faça um Merge Request.

Bom desenvolvimento!

Duvidas entrar em contato com o e-mail: contato@farmaja.com

