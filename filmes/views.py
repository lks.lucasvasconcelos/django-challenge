import requests
from django.shortcuts import render
# Create your views here.

def filme_list(request):

    name = request.POST['search']
    filmes = requests.get(url="http://www.omdbapi.com/?s="+name+"&apikey=749624c6").json()['Search'] 

    for filme in filmes:
        filme['data'] = requests.get(url="http://www.omdbapi.com/?i="+filme['imdbID']+"&apikey=749624c6").json()
        # print(filme['nota'])

    return render(request, 'filme_list.html', {'filmes': filmes})

def filme_busca(request):
    return render(request, 'filme_busca.html')