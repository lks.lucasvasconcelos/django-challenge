from django.urls import path
from . import views

urlpatterns = [
    path('', views.filme_busca, name='filme_busca'),
    path('listar/', views.filme_list, name='filme_list')
]